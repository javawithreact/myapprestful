package info.froylanvillaverde.myapprestful.model;

public class AuthenticationBean {

    public AuthenticationBean(String message) {
        this.message = message;
    }

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
