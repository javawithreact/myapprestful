package info.froylanvillaverde.myapprestful.web;

import info.froylanvillaverde.myapprestful.dao.TodoJpaRepository;
import info.froylanvillaverde.myapprestful.model.Todo;
import info.froylanvillaverde.myapprestful.service.TodoHarcodedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TodoJpaResource {

    @Autowired
    private TodoHarcodedService todoService;

    @Autowired
    private TodoJpaRepository repository;

    @GetMapping("/jpa/users/{username}/todos")
    public List<Todo> getAllTodos(@PathVariable String username) throws InterruptedException {
        return repository.findByName(username);
    }

    @GetMapping("/jpa/users/{username}/todos/{id}")
    public Todo getTodo(@PathVariable String username, @PathVariable long id) {
        return repository.findById(id).get();
    }

    @DeleteMapping("/jpa/users/{username}/todos/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable String username,@PathVariable long id){
        repository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/jpa/users/{username}/todos/{id}")
    public ResponseEntity<Todo> updateTodo(@PathVariable String username,@PathVariable long id, @RequestBody Todo todo){
        todo.setName(username);
        Todo todoUpdated = repository.save(todo);
        return new ResponseEntity<Todo>(todoUpdated, HttpStatus.NO_CONTENT);
    }

    @PostMapping("/jpa/users/{username}/todos")
    public ResponseEntity<Void> createTodo(@PathVariable String username, @RequestBody Todo todo){
        todo.setName(username);
        Todo createdTodo = repository.save(todo);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdTodo.getId())
        .toUri();
        return ResponseEntity.created(uri).build();
    }
}
