INSERT INTO todo (id, name, description, target_date, is_done) values (1001, 'freud', 'Learn JPA', sysdate(), false);
INSERT INTO todo (id, name, description, target_date, is_done) values (1002, 'freud', 'Learn microservices', sysdate(), false);
INSERT INTO todo (id, name, description, target_date, is_done) values (1003, 'freud', 'Learn react', sysdate(), false);