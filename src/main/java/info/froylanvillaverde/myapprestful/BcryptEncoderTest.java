package info.froylanvillaverde.myapprestful;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BcryptEncoderTest {

    public static void main(String[] args) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        for (int i=0; i<=10; i++){
            String encodedString = passwordEncoder.encode("password");
            System.out.println(encodedString);
        }
    }
}
