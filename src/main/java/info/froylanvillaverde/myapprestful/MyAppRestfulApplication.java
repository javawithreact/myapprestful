package info.froylanvillaverde.myapprestful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyAppRestfulApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyAppRestfulApplication.class, args);
    }

}
