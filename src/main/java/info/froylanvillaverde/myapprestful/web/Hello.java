package info.froylanvillaverde.myapprestful.web;

import info.froylanvillaverde.myapprestful.model.HelloWorldBean;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class Hello {

    @GetMapping("/hello")
    public String getMessage(){
        return "Hello world!!";
    }

    @GetMapping("/hello-world-bean")
    public HelloWorldBean helloWorldBean(){
        return new HelloWorldBean("Hello world");
    }

    @GetMapping("/hello-world-bean/path-variable/{name}")
    public HelloWorldBean helloWorldPathVariable(@PathVariable String name){
        //throw new RuntimeException("Something went wrong");
        return new HelloWorldBean(String.format("Hello world, %s", name));
    }
}
