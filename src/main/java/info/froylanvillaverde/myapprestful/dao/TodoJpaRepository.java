package info.froylanvillaverde.myapprestful.dao;

import info.froylanvillaverde.myapprestful.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoJpaRepository extends JpaRepository<Todo, Long> {

    List<Todo> findByName(String name);
}
